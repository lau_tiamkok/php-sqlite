<?php

/*
* Code to query an SQLite database and return
* results as JSON.
* @ ref: https://nyquist212.wordpress.com/2014/03/28/example-to-connect-to-sqlite-with-php-and-return-json/
* @ ref: http://php.net/manual/en/pdostatement.fetch.php
*/

header('Content-Type: application/json');

// Specify your sqlite database name and path.
$dir = 'sqlite:testDB.sqlite3';

// Instantiate PDO connection object and failure msg.
$dbh = new PDO($dir) or die("cannot open database");

// Define your SQL statement //
$query = "SELECT * FROM users";

// Iterate through the results and pass into JSON encoder.
foreach ($dbh->query($query) as $row) {
    //var_dump($row);
    //echo json_encode($row);
}

// Returns an array containing all of the result set rows.
$sth = $dbh->prepare("SELECT * FROM users");
$sth->execute();
$rows = $sth->fetchAll(PDO::FETCH_ASSOC);
//var_dump($rows);

// Iterate through the results and pass into JSON encoder.
foreach ($rows as $row) {
    //var_dump($row);
    echo json_encode($row) . "\n";
}
